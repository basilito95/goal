var videoDurationPlugin = {
    createEvent: function(duration, successCallback, errorCallback) {
        cordova.exec(
            successCallback, // success callback function
            errorCallback, // error callback function
            'GetVideoDuration', // mapped to our native Java class called "GetVideoDuration"
            'getVideoDuration', // with this action name
            [duration]
        ); 
     }
}