//wait for device to be ready
document.addEventListener('deviceready', onDeviceReady, false);

var user = {};
var networkstate;
var uploading = false;
var ft;
var newvideo = {};
var opennedlist;

var navPanelOpened = false;
var settingPanelOpened = false;

function onDeviceReady(){
  console.log("ready");
  checkConnection();
    //main js goes here
    //check if the user logged in before
    //NativeStorage.getItem("reference", this.getSuccess, this.getError);
    NativeStorage.getItem("UserSession", function(obj){
      user = obj;
      AdjustToUser(obj);
      $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#home");
    }, function(error){
      console.log(error);
      if(error.code == 2) $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#login");
    });

    //handling back button press
    document.addEventListener("backbutton", onBackKeyDown, false);
    document.addEventListener("offline", onOffline, false);
    document.addEventListener("online", onOnline, false);

    $( "#leftpanel" ).on( "panelopen", function( event, ui ) {
      navPanelOpened = true;
    } );

    $( "#leftpanel" ).on( "panelclose", function( event, ui ) {
      navPanelOpened = false;
    } );

    $( "#bottomsheet" ).on( "panelopen", function( event, ui ) {
      settingPanelOpened = true;
    } );

    $( "#bottomsheet" ).on( "panelclose", function( event, ui ) {
      settingPanelOpened = false;
    } );
}

function checkConnection() {
    if(navigator.connection.type == Connection.NONE)
      networkstate = false;
    else
      networkstate = true;
}

function onOffline() {
  // Handle the offline event
  networkstate = false;
}

function onOnline() {
    // Handle the online event
    networkstate = true;
}

function onBackKeyDown(e){
  console.log("back pressed");
  $('video').each(function(){this.pause();});
  if(navPanelOpened){
    $( "#leftpanel" ).panel("close");
  }
  else if(settingPanelOpened){
    $( "#bottomsheet" ).panel("close");
  }
  else if($.mobile.activePage.is('#login') || $.mobile.activePage.is('#home')){
    //if the page checking doesnt work try:
    // $.mobile.activePage.attr("id") == '#dept-list-page'
    e.preventDefault();
    navigator.app.exitApp();
  }
  else if(uploading){
    ft.abort();
    $( "#uploadpopup" ).popup( "close" );
  }else{
    history.back();
  }
}

$( "#loginform" ).submit(function( event ) {
  event.preventDefault();
    //login function
    //first catch data
  if(networkstate){
    $("#loginitem").remove();
    var username = $('#login-username').val();
    var pass = $('#login-password').val();
    var remember = $('#login-rememberme').is(':checked');
    var login = false;

    if(username == '' || pass == ''){
      console.log("error");
    }
    else{
      ActivityIndicator.show("Logging in...");
      var postdata = {
        'username' : username,
        'pass' : pass
      }
      //begin checking
      $.get('http://goal.technosaab.com/api/users/login_video/',postdata,function(response){
        ActivityIndicator.hide();
        if(response.status == "ok"){
          user = {
            id: response.ID,
            name: response.display_name,
            email: response.user_email,
            pic: response.pic,
            points:response.user_points,
            videos:response.videos
          };
          if(remember){
            NativeStorage.setItem("UserSession", user, function(user){
              console.log("Done");
            }, function(error){
              console.log(error.code);
              if (error.exception !== "") console.log(error.exception);
            });
          }
          AdjustToUser(user);
          $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#home");
        }
        else{
          alert("Wrong information");
        }
      },'json');
    }
    $('#login-username').val('');
    $('#login-password').val('');
  }
  else{
    alert("your are offline please Connect to the internet and try agian");
  }
});

function SignOut(){
  NativeStorage.getItem("UserSession", function(){
    NativeStorage.remove("UserSession",function () {
        console.log("Removed");
    },function (error) {
        console.log(error.code);
        if (error.exception !== "") console.log(error.exception);
    });
  }, function(error){
    console.log(error.code);
    if (error.exception !== "") console.log(error.exception);
  });
    user = {};
    $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#login");
}

function AdjustToUser(obj){
  $("#mychannelvideos").empty();
  $("#ProfilePic").attr("src",obj.pic);
  $('#UsernameWelcome').html(obj.name);
  $('#UserPoints').html("Points: " + obj.points);
  obj.videos.forEach(function(item, index){
    var text = "<li class='ui-li-has-thumb ui-li-static ui-body-inherit' onclick='playvideo("+'"'+item.link+'")'+"'"+'><img src="img/football.png" class="ui-thumbnail"><h2>'+item.title+'</h2><p>'+item.date+'</p><p>Points: '+item.points+'</p></li>';
    $("#mychannelvideos").append(text);
  });
}

function updatecache(){
  NativeStorage.getItem("UserSession", function(){
    NativeStorage.remove("UserSession",function () {
        console.log("Removed");
    },function (error) {
        console.log(error.code);
        if (error.exception !== "") console.log(error.exception);
    });
    NativeStorage.setItem("UserSession", user, function(user){
        console.log("Done");
      }, function(error){
        console.log(error.code);
        if (error.exception !== "") console.log(error.exception);
      });
  }, function(error){
    console.log(error.code);
    if (error.exception !== "") console.log(error.exception);
  });
}

function uploadvideo(){
  if(user.id == -1){
    alert("You have to login to be able to upload videos");
  }
  else{
    navigator.camera.getPicture(onCameraSuccess, onCameraFail, { quality: 100,
      destinationType: Camera.DestinationType.FILE_URI,
      sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: Camera.MediaType.VIDEO
    });
  }
}

function onCameraSuccess(response){
  var duration = 0;
  var success = function(message) {
      duration = parseInt(message)/1000;
      if(duration < 31){//upload
        console.log("uploading");
        //show form
        $("#fileurl").val(response);
        $( "#uploadform" ).popup("open",{ positionTo: "window" });
      }
      else{
        alert("Error! : You are only allowed to upload 30 Seconds videos trim the video and try agian");
      }
      //playvideo(response);
  }

  var failure = function() {
      console.log("Error calling Hello Plugin");
  }

  hello.greet(response, success, failure);   
}

function onCameraFail(response){
  console.log(response);
}

$( "#upform" ).submit(function( event ) {
  event.preventDefault();
    //login function
    //first catch data
  if(networkstate){
    var posttitle = $("#posttitle").val();
    var catid = $("#catid").val();
    var url = $("#fileurl").val();
    $( "#uploadform" ).popup("close");
    postVideo(url,posttitle,catid);
  }
  else{
    alert("You are not connected to internet");
  }
});

function playvideo(url){
  // Play a video with callbacks
  var options = {
    successCallback: function() {
      console.log("Video was closed without error.");
    },
    errorCallback: function(errMsg) {
      console.log("Error! " + errMsg);
    }
  };
  window.plugins.streamingMedia.playVideo(url, options);
}

function postVideo(fileURI,title,cat) {
  /*var metadata = {
    snippet: {
      title: "test",
      description: "test",
      tags: ["youtube-cors-upload"],
      categoryId: 21
    },
    status: {
      privacyStatus: "unlisted"
    }
  }

  var options = new FileUploadOptions();
  options.fileKey = "file";
  options.fileName = 'test';
  options.mimeType = "video/mp4";
  options.chunkedMode = false;

  options.headers = {
    Authorization: "Bearer " + accessToken,
    "Access-Control-Allow-Origin": "http://meteor.local"
  };

  var params = new Object();
  params.part = Object.keys(metadata).join(',')

  options.params = params;
  console.log(options)
  ft = new FileTransfer();
  ft.upload(fileURI, "https://www.googleapis.com/upload/youtube/v3/videos?part=snippet", win, fail, options, true);*/
  var options = new FileUploadOptions();
  options.fileKey="file";
  options.fileName=fileURI.substr(fileURI.lastIndexOf('/')+1);
  options.mimeType="video/mp4";

  var params = new Object();
  params.title = title;
  params.cat = catid;
  params.user_id = user.id;


  options.params = params;
  options.chunkedMode = false;

  var ft = new FileTransfer();
  ft.upload(fileURI, "http://goal.technosaab.com/api/users/add_video/", win, fail, options);
  uploading = true;
  newvideo.title = title;
  newvideo.points = 0;
  $( "#uploadpopup" ).popup("open",{ positionTo: "window" });

  ft.onprogress = function(progressEvent) {
    if (progressEvent.lengthComputable) {
       console.log(progressEvent)
       //loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
    } else {
      console.log('something not loading')
      //loadingStatus.increment();
    }
    $("#percent").html(Math.round(progressEvent.loaded / progressEvent.total * 100));
  };
}

function win(r) {
  console.log(r)
  console.log("Code = " + r.responseCode);
  console.log("Response = " + r.response);
  console.log("Sent = " + r.bytesSent);
  var obj = JSON.parse(r.response);
  uploading = false;
  $( "#uploadpopup" ).popup( "close" );
  alert("Uploaded Successfully");
  newvideo.date = obj.time;
  newvideo.link = obj.url;
  user.videos.push(newvideo);
  updatecache();
  AdjustToUser(user);
  $("#posttitle").val('');
  $("#catid").val('');
  $("#fileurl").val('');
}

function fail(error) {
  console.log(error)
    // alert("An error has occurred: Code = " + error.code);
  console.log("upload error source " + error.source);
  console.log("upload error target " + error.target);
  uploading = false;
  $( "#uploadpopup" ).popup( "close" );
  alert("Error");
}

function FillList(list){
  switch(list){
    case 0:
      opennedlist = 0;
      ActivityIndicator.show("Loading...");
      var postdata = {};
      if(user.id != -1){
        postdata = {
          'userid' : user.id
        };
      }
      //begin checking
      $.get('http://goal.technosaab.com/api/users/all_in_one/',postdata,function(response){
        ActivityIndicator.hide();
        if(response.status == "ok"){
          $("#videolisttitle").html("Best Goals");
          $("#listvideos").empty();
          response.best.forEach(function(item, index){
            if(item.photo == null){
              item.photo = 'img/profilepic.jpeg';
            }
            var text = '<div class="nd2-card">'+
              '<div class="card-title has-avatar">'+
                '<div class="row between-xs">'+
                  '<div class="col-xs-8">'+
                    '<div class="box">'+
                      '<img class="card-avatar" src="' + item.photo + '">'+
                      '<h3 class="card-primary-title">' + item.title + '</h3>'+
                      '<h5 class="card-subtitle">By: ' + item.name + '</h5>'+
                    '</div>'+
                  '</div>'+
                  '<div class="col-xs-4 align-right">'+
                    '<div class="box">'+
                      '<a href="#" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" onclick="follow('+item.author+')">Follow</a>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
              '<div class="card-media">'+
                '<video width="100%" controls preload="metadata">'+
                  '<source src="' + item.link + '" type="video/mp4">'+
                '</video>'+
              '</div>'+
              '<div class="card-supporting-text has-action">'+
                'Points: '+item.points+''+
                '<!-- RATING - Form -->'+
                '<fieldset class="rating" id="'+item.id+'">'+
                    '<input data-role="none" type="radio" name="rating'+item.id+'" id="str3" value="3"><label for="str3"><span onclick="rate('+item.id+',3);">&#9733</span></label>'+
                    '<input data-role="none" type="radio" name="rating'+item.id+'" id="str2" value="2"><label for="str2"><span onclick="rate('+item.id+',2);">&#9733</span></label>'+
                    '<input data-role="none" type="radio" name="rating'+item.id+'" id="str1" value="1"><label for="str1"><span onclick="rate('+item.id+',1);">&#9733</span></label>'+
                '</fieldset>'+
              '</div>'+
              '<div class="card-action">'+
                '<div class="row between-xs">'+
                  '<div class="col-xs-4">'+
                    '<div class="box">'+
                      '<a href="#" class="ui-btn ui-btn-inline ui-btn-fab waves-effect waves-button waves-effect waves-button"><i class="zmdi zmdi-facebook"></i></a>'+
                      '<a href="#" class="ui-btn ui-btn-inline ui-btn-fab waves-effect waves-button waves-effect waves-button"><i class="zmdi zmdi-twitter"></i></a>'+
                    '</div>'+
                  '</div>'+
                  '<div class="col-xs-8 align-right">'+
                    '<div class="box">'+
                      '<a href="#" class="ui-btn ui-btn-inline waves-effect waves-button waves-effect waves-button" onclick="retweet('+item.id+')">Retweet</a>'+
                    '</div>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>';
            $("#listvideos").append(text);
          });
          $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#videolist");
          $('<style></style>').appendTo($(document.body)).remove();
        }
        else{
          alert("something went Wrong");
        }
      },'json');
      break;
  }
}

function rate(vid,point){
  if(user.id == -1){
    alert("You have to login to be able to vote");
  }
  else{
    console.log(vid);
    console.log(point);
  }
}

function tryapp(){
  user = {
    id:-1,
    name:'guest',
  };
  $('#UsernameWelcome').html(user.name);
  $("#navbar").append('<li><a href="#login" class="ui-btn waves-effect waves-button" id="loginitem"><span class="navtxt">Login</span></a></li>');
  $( ":mobile-pagecontainer" ).pagecontainer( "change", "index.html#home");
}

function retweet(vid){
  if(user.id == -1){
    alert("You have to login to be able to retweet");
  }
  else{
    console.log(vid);
  }
}

function follow(uid){
  if(user.id == -1){
    alert("You have to login to be able to Follow Users");
  }
  else{
    console.log(uid);
  }
}